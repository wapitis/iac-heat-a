#!/bin/bash -v

export DEBIAN_FRONTEND=noninteractive

# install Puppet agent
tempdeb=$(mktemp /tmp/debpackage.XXXXXXXXXXXXXXXXXX) || exit 1

i=0;
wget -O "$tempdeb" https://apt.puppetlabs.com/puppet6-release-bionic.deb
ret="$?"
while [ "$i" -lt "5" ] && [ "$ret" -ne "0" ]; do
  sleep 10
  wget -O "$tempdeb" https://apt.puppetlabs.com/puppet6-release-bionic.deb
  ret="$?"
  let "i++"
done
if [ "$ret" -ne "0" ]; then # All attempts to download file failed, instruct clound-init to restart and try again
  exit 1003
fi

dpkg -i "$tempdeb"
apt-get update
apt-get -y install puppet-agent
echo "$(/opt/puppetlabs/bin/facter networking.ip) $(hostname).node.consul $(hostname)" >> /etc/hosts
echo "manager_ip_address manager.node.consul manager" >> /etc/hosts
/opt/puppetlabs/bin/puppet config set server manager.node.consul --section main
/opt/puppetlabs/bin/puppet config set runinterval 300 --section main
/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjk+Gyz5wKho+hFxeqzjQyCcLwzINaYXWyRgjnD1UQbXCgXNR4DyElwwo0nom2TZhc8gk1bBGuY5duCXrqtT8xNl+7lgA7niTxupVJ4/qfFo4WZ6gdtBPYdi6XlpNI1NzVyoPbtledIeBeJb55j91zzvoi1TvjV164lJrG0og7j/O8mdwo2wKfu3m7t6RZbpONi3YhXWzq7+GeGfd4YlgiqIilHajOZCFqxTkQY65iP2q8UlNL95/zifLEelVPe5UU8QYfpYsJnONgPTLFanMinGcbwIDz9aiREbAAtnW1dABCCTlwPkgPL0cpW7i8d0j8oh2oIfOBN3Lx4qn6WHH7 alfpet@loginstud01" >> /home/ubuntu/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCi5gtqEAGCJLf/eizGsP8ZzBwYGH2+UxpIP1hDweYOzGtN2YU+RminDG/dLRlGQ9N2PVRc3dA5ZVHhTKCSridmmtumGuna/dR2mFdU01kZKfJ1sR0u9EOXBXYEX7taHW0tnJU6vDIDQG2/iCPYuYNXGKnc91ZftlEXBbslGo1DTYMXlJTxBw2ID33CiEgYY+sWs6KiLutM5raLcnXUUk2J5fnLkEigqIgUqrRw3OcutJxrxicgmYb2ny59qiRCDUhvA6w2sI1jirqXFQPzTr85kJFVB5a59cphWV64HyJGJrPLf0AH+JmdQK4VPE0+rnAkGrUtICRK5DH1ocILRqPd simonrr@loginstud01" >> /home/ubuntu/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYaduxAF9fQsTNYhu4PFDocrzMOCK/ovWp9f+gPPSitkBXS6B60ubBX9kwXbSoTT8Mep1EnGr5ms0ZgqdtNVn0q+2BuJuPHYg9KddyZiWT0Rl7fWnf0ZhWIg5AelPfNS6Gd8Fl5CsiLIat7IcGiGnvpeqYCrGT4kcppExSQWQ5/wJGGsEODO8cQD6f8bv3CXggfcSNFV2XRsBq+Pr+GAtcEkQ5cL00TdRKdpoTB1K8IFrE8qLeyibzRoQTCzYHTzCdu/RuKyhw5l2geuNlMtaCJp5IDOf1Y5RvZOa/RsxD8dUIzl20e/f38jjiWweoABcEfIjDXNCOQK5XNeV/Cdb7 anderwmo@loginstud01" >> /home/ubuntu/.ssh/authorized_keys
