# IaC-Heat-A

This is a Heat template to launch a flexible provisioned infrastructure. The servers are initialized based on [this Puppet control repo](https://gitlab.com/erikhje/control-repo-a).

Clone and launch in OpenStack with e.g.
```bash
# edit iac_top_env.yaml and enter name of your keypair
git clone https://gitlab.com/erikhje/iac-heat-a.git
cd iac-heat-a
openstack stack create my_iac -t iac_top.yaml -e iac_top_env.yaml
```

This heat-template is modified with new security groups and different nodes then the original repo. The configurations here made in order to fit a elastic stack deployment in openstack. Link to [this puppet repo](https://gitlab.com/wapitis/elasticstack).

Node name beatslin and beatswin are representations of generic end nodes in a infrastructure.
